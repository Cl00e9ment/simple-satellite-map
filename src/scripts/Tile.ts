import $ from 'jquery'
import { Transform } from './Transform'

export class Tile {

	public static readonly SIZE = 512

	private x: number
	private y: number
	private zoom: number
	private element: JQuery<HTMLDivElement>
	private mounted = false

	public constructor(x: number, y: number, zoom: number) {
		this.x = x
		this.y = y
		this.zoom = zoom
		this.element = $(`<div class="tile"><img src="${this.getImageUrl()}"></div>`)
	}

	public render(transform: Transform, displaySize: number) {
		if (!this.mounted) this.mount()
		const xOffset = transform.xOffset + this.x * displaySize
		const yOffset = transform.yOffset + this.y * displaySize
		this.element.css('width', displaySize)
		this.element.css('height', displaySize)
		this.element.css('left', xOffset + 'px')
		this.element.css('top', yOffset + 'px')
	}

	public hide() {
		if (this.mounted) this.unmount()
	}

	private mount() {
		$('main').append(this.element)
		this.mounted = true
	}

	private unmount() {
		this.element.remove()
		this.mounted = false
	}

	private getImageUrl(): string {
		const loadBalancer = 1 + ((this.x + this.y) % 4)
		const apiKey = '0veR2C1rMPtEb7VtWwr-QTiZZuhIAgbYSqHiXbMIBJo'
		return `https://${loadBalancer}.aerial.maps.ls.hereapi.com/maptile/2.1/maptile/newest/satellite.day/${this.zoom}/${this.x}/${this.y}/${Tile.SIZE}/jpg?apiKey=${apiKey}`
	}
}
