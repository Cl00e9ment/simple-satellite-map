import { Tile } from './Tile'

export class Transform {

	private static readonly MIN_SCALE = 1

	private _scale = 1
	private _xOffset = 0
	private _yOffset = 0

	public center(container: JQuery) {
		this._scale = Math.min(container.width(), container.height()) / Tile.SIZE
		const tileDisplaySize = Tile.SIZE * this._scale
		this._xOffset = (container.width() - tileDisplaySize) / 2
		this._yOffset = (container.height() - tileDisplaySize) / 2
	}

	public onWheel(mouseX: number, mouseY: number, deltaY: number) {
		const oldScale = this._scale
		this._scale /= deltaY / 100 + 1
		this._scale = Math.max(this._scale, Transform.MIN_SCALE)

		const factor = this._scale / oldScale
		this._xOffset -= (mouseX - this._xOffset) * (factor - 1)
		this._yOffset -= (mouseY - this._yOffset) * (factor - 1)
	}

	public onResize(deltaW: number, deltaH: number) {
		this._xOffset += deltaW / 2
		this._yOffset += deltaH / 2
	}

	public onDrag(deltaX: number, deltaY: number) {
		this._xOffset += deltaX
		this._yOffset += deltaY
	}

	get scale(): number {
		return this._scale
	}

	get xOffset(): number {
		return this._xOffset
	}

	get yOffset(): number {
		return this._yOffset
	}
}