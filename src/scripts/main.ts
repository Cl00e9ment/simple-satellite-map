import $ from 'jquery'
import { Tile } from './Tile'
import { Transform } from './Transform'
import { clamp } from './utils'

const win = $(window)
const mainContainer = $('main')
const overlay = $('#overlay')
const tiles: Tile[][][] = []
const transform = new Transform()
let previouslyRenderedTiles: Tile[] = []
let waitingForRender = false

mainContainer.on('wheel', ({originalEvent}) => {
	const event = originalEvent as WheelEvent
	const containerPos = mainContainer.position()
	const x = event.clientX - containerPos.left
	const y = event.clientY - containerPos.top
	transform.onWheel(x, y, event.deltaY)
	requestRender()
})

let dragX = NaN
let dragY = NaN
overlay.on('dragover', ({originalEvent}) => {
	const event = originalEvent as DragEvent
	const newX = event.pageX
	const newY = event.pageY
	if (isNaN(dragX) || isNaN(dragY)) {
		dragX = newX
		dragY = newY
		return
	}
	const deltaX = newX - dragX
	const deltaY = newY - dragY
	if (deltaX === 0 || deltaY === 0) return
	transform.onDrag(deltaX, deltaY)
	requestRender()
	dragX = newX
	dragY = newY
})
overlay.on('dragend', () => {
	dragX = NaN
	dragY = NaN
})

let winW = win.width()
let winH = win.height()
win.on('resize', () => {
	const newW = win.width()
	const newH = win.height()
	const deltaW = newW - winW
	const deltaH = newH - winH
	transform.onResize(deltaW, deltaH)
	requestRender()
	winW = newW
	winH = newH
})

transform.center(mainContainer)
requestRender()

function requestRender() {
	if (waitingForRender) return
	waitingForRender = true
	requestAnimationFrame(() => {
		render()
		waitingForRender = false
	})
}

function render() {
	const zoom = clamp(1, Math.ceil(Math.log2(transform.scale)), 19)
	const tilesPerEdge = 2**zoom

	const tileDisplaySize = Tile.SIZE / tilesPerEdge * transform.scale
	let minX = Math.floor(-transform.xOffset / tileDisplaySize)
	let maxX = Math.floor((mainContainer.width() - transform.xOffset) / tileDisplaySize + 1)
	let minY = Math.floor(-transform.yOffset / tileDisplaySize)
	let maxY = Math.floor((mainContainer.height() - transform.yOffset) / tileDisplaySize + 1)
	minX = clamp(0, minX, tilesPerEdge)
	maxX = clamp(0, maxX, tilesPerEdge)
	minY = clamp(0, minY, tilesPerEdge)
	maxY = clamp(0, maxY, tilesPerEdge)

	const renderedTiles = []

	for (let x = minX; x < maxX; x++) {
		for (let y = minY; y < maxY; y++) {
			const tile = getTile(x, y, zoom)
			tile.render(transform, tileDisplaySize)
			renderedTiles.push(tile)
		}
	}

	// hide tiles that are no longer rendered
	for (const previouslyRenderedTile of previouslyRenderedTiles) {
		if (!renderedTiles.includes(previouslyRenderedTile)) {
			previouslyRenderedTile.hide()
		}
	}

	previouslyRenderedTiles = renderedTiles
}

function getTile(x: number, y: number, zoom: number): Tile {
	let map = tiles[zoom]
	if (map === undefined) map = tiles[zoom] = []

	let column = map[x]
	if (column === undefined) column = map[x] = []

	let tile = column[y]
	if (tile === undefined) tile = column[y] = new Tile(x, y, zoom)

	return tile
}
